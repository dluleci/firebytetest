
using UnityEngine;

public class MenuInput : MonoBehaviour
{
    void Update()
    {
        if (GameManager.INSTANCE.gameState == GameState.MENU)
        {
            PlayerController pc = GameManager.INSTANCE.player.GetComponent<PlayerController>();
            if (pc.isInitPlayer && pc.isInitPosition)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    GameManager.INSTANCE.gameState = GameState.PLAY;
                    GameManager.INSTANCE.PerformChangeState();
                }
            }
        }
    }
}