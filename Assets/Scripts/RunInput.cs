
using UnityEngine;

public class RunInput : MonoBehaviour
{
    public Transform player;
    public PlayerMovement playerMove;
    public Transform weapon;

    private Vector3 firstPos = Vector3.zero;
    private Vector3 secondPos = Vector3.zero;

    public float horizontalSpeedMultiplier;

    void Update()
    {
        if (GameManager.INSTANCE.gameState == GameState.PLAY)
        {
            if (Input.GetMouseButton(0))
            {
                weapon.GetComponent<WeaponObj>().canShot = true;
                firstPos = Input.mousePosition;
                if (secondPos != Vector3.zero)
                {
                    Vector3 differenceVector = firstPos - secondPos;
                    differenceVector.y = 0f;
                    if (differenceVector.magnitude > 0.05f)
                    {
                        differenceVector.x = (differenceVector.x * 100f) / GameManager.INSTANCE.width;
                        playerMove.canMoveHorizontal = true;
                        playerMove.horizontalSpeed = differenceVector.x * horizontalSpeedMultiplier;
                    }
                    else
                    {
                        playerMove.canMoveHorizontal = false;
                        playerMove.horizontalSpeed = 0f;
                    }
                }
                secondPos = Input.mousePosition;
            }
            if (Input.GetMouseButtonUp(0))
                Reset();
        }
    }

    public void Reset()
    {
        weapon.GetComponent<WeaponObj>().canShot = false;
        firstPos = Vector3.zero;
        secondPos = Vector3.zero;
        playerMove.canMoveHorizontal = false;
        playerMove.horizontalSpeed = 0f;
    }
}