﻿
using UnityEngine;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    public static GameManager INSTANCE;

    [HideInInspector] public int width, height;
    [HideInInspector] public bool isAssetsLoaded = false;
    [HideInInspector] public bool isGameManagerLoaded = false;

    [HideInInspector] public GameState gameState;

    public LevelManager levelManager;
    public UIController uiController;

    public Transform player;
    public Transform enemy;

    private void Awake()
    {
        if (INSTANCE != null && INSTANCE != this)
        {
            Destroy(this.gameObject);
            return;
        }
        INSTANCE = this;
        DontDestroyOnLoad(this.gameObject);

        gameState = GameState.LOAD;

        width = Screen.width;
        height = Screen.height;
        if (Utils.IS_DEBUG_MODE)
            PlayerPrefs.DeleteAll();
        DOTween.Init();
    }

    void Start()
    {
        Application.targetFrameRate = 60;
        InitGame();
    }

    public void InitGame()
    {
        levelManager.CreateLevel(); 
        isGameManagerLoaded = true;
    }

    void Update()
    {

    }

    public void PerformChangeState()
    {
        uiController.isStateInit = false;
    }

    public void RestartGame()
    {
        levelManager.PerformReset();
    }
}