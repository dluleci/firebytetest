﻿
public enum GameState
{
    LOAD,
    MENU,
    PLAY,
    WIN,
    FAIL
}