
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public PlayerAnimator playerAnimator;

    public Transform objectHolder;
    [HideInInspector] public Transform body;

    [HideInInspector] public bool isInitPlayer = false;
    [HideInInspector] public bool isInitPosition = false;

    bool isSlowedDown = false;
    float slowDownStartTime;
    public float slowDownTimeframe;

    [HideInInspector] public int killCount = 0;

    void Start()
    {
        Init();
    }

    public void Init()
    {
        killCount = 0;
        body = objectHolder.GetChild(0);
        playerAnimator.Init();
        playerAnimator.Reset();
        isInitPlayer = true;
        body.rotation = Quaternion.identity; 
        isSlowedDown = false;
        GetComponent<PlayerMovement>().canGetFaster = true;
    }

    public void SetPosition(Transform spawnPoint)
    {
        transform.position = spawnPoint.position;
        transform.rotation = spawnPoint.rotation;
        isInitPosition = true;
    }

    void Update()
    {
        switch (GameManager.INSTANCE.gameState)
        {
            case GameState.PLAY:
                if (isInitPlayer)
                    isInitPlayer = false;
                if (isInitPosition)
                    isInitPosition = false;

                if (isSlowedDown)
                {
                    if (Time.time >= slowDownStartTime + slowDownTimeframe)
                    {
                        isSlowedDown = false;
                        GetComponent<PlayerMovement>().canGetFaster = true;
                    }
                }
                print(GameManager.INSTANCE.levelManager.currentEnvironment
                                .GetComponent<LevelProps>().enemyGenerator
                                .GetComponent<EnemyGenerator>().enemyCount);

                print("KC: " + killCount);

                if (killCount == GameManager.INSTANCE.levelManager.currentEnvironment
                                .GetComponent<LevelProps>().enemyGenerator
                                .GetComponent<EnemyGenerator>().enemyCount)
                {
                    playerAnimator.Win();
                    GameManager.INSTANCE.gameState = GameState.WIN;
                    GameManager.INSTANCE.PerformChangeState();
                }
                break;
        }
    }

    public void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.transform.tag.Equals(Tags.OBSTACLE))
        {
            isSlowedDown = true;
            GetComponent<PlayerMovement>().canGetFaster = false;
            slowDownStartTime = Time.time;
            Destroy(hit.gameObject);
            ObjectPooler.INSTANCE.SpawnFromPool("CubeBlow", transform.position, Quaternion.identity);
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag.Equals(Tags.FINISH))
        {
            if (GameManager.INSTANCE.gameState != GameState.FAIL)
            {
                playerAnimator.Win();
                GameManager.INSTANCE.gameState = GameState.WIN;
                GameManager.INSTANCE.PerformChangeState();
            }
        }
    }

    public void Die()
    {
        GameManager.INSTANCE.gameState = GameState.FAIL;
        GameManager.INSTANCE.PerformChangeState();
        playerAnimator.Die();
    }
}