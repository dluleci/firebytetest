
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;
    public PlayerAnimator animator;

    [HideInInspector] public bool canGetFaster = true;
    public float verticalHighSpeed;
    public float verticalSlowDownSpeed;
    public float smoothVerticalTime;

    [HideInInspector] public bool canMoveHorizontal = false;
    [HideInInspector] public float horizontalSpeed;
    public float horizontalSpeedClamp;
    public float smoothHorizontalTime;

    [HideInInspector] public float horizontal = 0f;
    float vertical = 0f;

    public float horizontalClampValue;

    public float turnSmoothTime;
    float turnSmoothVelocity;

    float currentVelocityForSlowDown = 0f;

    float gravityVelocity = 0f;
    public float gravity;

    void Update()
    {
        if (GameManager.INSTANCE.gameState == GameState.PLAY)
        {
            if (canGetFaster)
                vertical = Mathf.Lerp(vertical, verticalHighSpeed, smoothVerticalTime);
            else
                vertical = Mathf.SmoothDamp(vertical, verticalSlowDownSpeed, ref currentVelocityForSlowDown, 0.25f);

            if (canMoveHorizontal)
                horizontal = Mathf.Lerp(horizontal, horizontalSpeed, smoothHorizontalTime);
            else
                horizontal = Mathf.Lerp(horizontal, 0f, smoothHorizontalTime);
          
            if (horizontal >= horizontalSpeedClamp)
                horizontal = horizontalSpeedClamp;
            else if (horizontal <= -1f * horizontalSpeedClamp)
                horizontal = (-1f * horizontalSpeedClamp);

            CalculateGravity();
            Vector3 direction = new Vector3(horizontal, 0f, vertical);
            Vector3 fallVector = new Vector3(0, gravityVelocity, 0);
            direction += fallVector;

            if (direction.magnitude >= 0.1f)
                PerformMove(direction);

            float xValue = transform.position.x;
            xValue = Mathf.Clamp(xValue, -horizontalClampValue, horizontalClampValue);
            transform.position = new Vector3(xValue, transform.position.y, transform.position.z);
        }
        else if (GameManager.INSTANCE.gameState == GameState.FAIL || GameManager.INSTANCE.gameState == GameState.MENU ||
            GameManager.INSTANCE.gameState == GameState.LOAD)
        {
            CalculateGravity();
            vertical = Mathf.SmoothDamp(vertical, 0f, ref currentVelocityForSlowDown, 0.75f);
            horizontal = Mathf.Lerp(horizontal, 0f, smoothHorizontalTime / 2f);
            Vector3 direction = new Vector3(horizontal, 0f, vertical);
            Vector3 fallVector = new Vector3(0, gravityVelocity, 0);
            direction += fallVector;
            PerformMove(direction);
        }
    }

    private void CalculateGravity()
    {
        gravityVelocity -= gravity * Time.deltaTime;
        if (controller.isGrounded && gravityVelocity < 0)
            gravityVelocity = 0f;
    }

    private void PerformMove(Vector3 direction)
    {
        if (GameManager.INSTANCE.gameState == GameState.PLAY)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
        }
        controller.Move(direction * Time.deltaTime);
    }

    private float WrapAngle(float angle)
    {
        angle %= 360;
        if (angle > 180)
            return angle - 360;
        return angle;
    }
}