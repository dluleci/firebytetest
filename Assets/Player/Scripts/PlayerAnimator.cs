
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    Animator animator;
    public Transform objectHolder;

    public PlayerMovement movement;
    public PlayerController playerController;

    [HideInInspector] public float verticalAnimationSpeed;
    public float accelerateSpeedOnRun, decelerateSpeedOnRun;

    [HideInInspector] public float sneakSpeed;

    public void Init()
    {
        animator = objectHolder.GetChild(0).GetComponent<Animator>();
    }

    void Update()
    {
        if (animator)
        {
            switch (GameManager.INSTANCE.gameState)
            {
                case GameState.MENU:
                    break;
                case GameState.PLAY:
                    if (movement.canGetFaster)
                        verticalAnimationSpeed += accelerateSpeedOnRun * Time.deltaTime;
                    else
                        verticalAnimationSpeed -= decelerateSpeedOnRun * Time.deltaTime;
                    verticalAnimationSpeed = Mathf.Clamp(verticalAnimationSpeed, 0f, 1f);
                    animator.SetFloat("VerticalSpeed", verticalAnimationSpeed);
                    break;
            }
        }
    }

    public void Die()
    {
        animator.SetBool("isDied", true);
    }

    public void Win()
    {
        animator.SetBool("isWin", true);
    }

    public void Reset()
    {
        animator.SetBool("isWin", false);
        animator.SetFloat("VerticalSpeed", 0f);
        animator.SetBool("isDied", false);
    }
}