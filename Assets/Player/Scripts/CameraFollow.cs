
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public Transform lookTarget;
    public Vector3 offset;
    public float smoothTime;

    private Vector3 velocity = Vector3.zero;
    void Start()
    {
        Init();
    }

    void LateUpdate()
    {
        if (GameManager.INSTANCE.gameState == GameState.PLAY)
        {
            Vector3 targetPosition = target.position + offset;// target.TransformPoint(offset);
            targetPosition = new Vector3(transform.position.x, transform.position.y, targetPosition.z);
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        }
    }

    public void Init()
    {
        velocity = Vector3.zero;
        transform.position = target.TransformPoint(offset);
        transform.LookAt(lookTarget);
    }
}
