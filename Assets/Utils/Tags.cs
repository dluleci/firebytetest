
public class Tags
{
    public static string PLATFORM = "Platform";
    public static string FINISH = "Finish";
    public static string PLAYER = "Player";
    public static string KILLZONE = "Killzone";
    public static string OBSTACLE = "Obstacle";
    public static string ENEMY = "Enemy";
}