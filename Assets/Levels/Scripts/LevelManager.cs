
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public List<GameObject> environments = new List<GameObject>();
    [HideInInspector] public GameObject currentEnvironment = null;
    public int currentLevel = 0;

    public void CreateLevel()
    {
        currentLevel = PlayerPrefs.GetInt("Level", 0);

        int index = currentLevel;
        if (index >= environments.Count)
            index = index % environments.Count;

        if (currentEnvironment)
            Destroy(currentEnvironment);

        GameObject environment = environments[index] as GameObject;
        currentEnvironment = Instantiate(environment, Vector3.zero, environment.transform.rotation);
        Init();
    }

    public void Init()
    {
        GameManager gm = GameManager.INSTANCE;
        Transform playerRef = currentEnvironment.GetComponent<LevelProps>().GetPlayerSpawn();
        Transform finishRef = currentEnvironment.GetComponent<LevelProps>().GetFinishLevel();
        gm.player.GetComponent<PlayerMovement>().controller.enabled = false;
        gm.player.GetComponent<PlayerMovement>().horizontalClampValue = currentEnvironment.GetComponent<LevelProps>().horizontalClampValue;
        gm.player.GetComponent<PlayerController>().SetPosition(playerRef);
        gm.player.GetComponent<PlayerController>().Init();
        gm.player.GetComponent<PlayerMovement>().controller.enabled = true;
        Camera.main.GetComponent<CameraFollow>().Init();
    }

    public void PerformReset()
    {
        GameManager gm = GameManager.INSTANCE;
        gm.player.GetComponent<PlayerController>().Init();
        gm.GetComponent<RunInput>().Reset();
        gm.gameState = GameState.MENU;
        gm.PerformChangeState();
        CreateLevel();
    }
}