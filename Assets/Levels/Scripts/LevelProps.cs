
using UnityEngine;

public class LevelProps : MonoBehaviour
{
    public float horizontalClampValue;
    public Transform playerSpawnPoint;
    public Transform finishLevel;
    public Transform enemyGenerator;

    public Transform GetPlayerSpawn()
    {
        return playerSpawnPoint;
    }

    public Transform GetFinishLevel()
    {
        return finishLevel;
    }
}