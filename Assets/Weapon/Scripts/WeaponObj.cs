
using UnityEngine;

public class WeaponObj : MonoBehaviour
{
    public Transform anchor;
    public Transform projectile;

    public float shootingFrequency;
    bool isShooted = false;
    float shootedTime;

    [HideInInspector] public bool canShot;

    public void Init()
    {
        isShooted = false;

    }

    void Update()
    {
        transform.position = anchor.position;
        if(GameManager.INSTANCE.gameState == GameState.PLAY && canShot)
        {
            if (!isShooted)
            {
                isShooted = true;
                shootedTime = Time.time;
                ObjectPooler.INSTANCE.SpawnFromPool("Bullet", projectile.position, Quaternion.identity);
            }
            else
            {
                if(Time.time >= shootedTime + shootingFrequency)
                {
                    isShooted = false;
                }
            }
        }
    }
}
