
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    GameManager gm;

    void Start()
    {
        gm = GameManager.INSTANCE;
    }

    public void OnStart()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().AddForce(transform.forward * speed * -1f);
        Invoke("DisableObject", 2.5f);
    }

    private void DisableObject()
    {
        gameObject.SetActive(false);
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        transform.rotation = Quaternion.identity;
        transform.position = Vector3.zero;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (gm.gameState == GameState.PLAY)
        {
            if (collider.transform.tag.Equals(Tags.ENEMY))
            {
                collider.GetComponent<EnemyController>().Die();
            }
        }
    }
}