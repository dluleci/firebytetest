﻿
using UnityEngine;

public class SplashScreen : MonoBehaviour
{
    public GameObject loadingText;
    public bool isSplashOn;

    void Start()
    {
        if (isSplashOn)
            Invoke("CloseSplash", 2.5f);
        else
        {
            gameObject.SetActive(false);
            GameManager.INSTANCE.gameState = GameState.MENU;
            GameManager.INSTANCE.PerformChangeState();
        }
    }

    private void CloseSplash()
    {
        if (GameManager.INSTANCE.isAssetsLoaded && GameManager.INSTANCE.isGameManagerLoaded)
        {
            GameManager.INSTANCE.gameState = GameState.MENU;
            GameManager.INSTANCE.PerformChangeState();
            gameObject.SetActive(false);
        }
        else
        {
            Invoke("CloseSplash", 2.5f);
            loadingText.SetActive(true);
        }
    }
}