﻿
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Progress : MonoBehaviour
{
    public Transform player;
    private Transform target;

    private float fullDistance;
    public Image fillBar;

    public Text currentLevelText, targetLevelText;

    void Start()
    {
        Init(GameManager.INSTANCE.levelManager.currentEnvironment.GetComponent<LevelProps>().GetFinishLevel());
    }

    void Update()
    {
        if (target != null)
        {
            switch (GameManager.INSTANCE.gameState)
            {
                case GameState.PLAY:
                    float currentDistance = Vector3.Distance(player.transform.position, target.transform.position);
                    float scaleFactor = (fullDistance - currentDistance) / fullDistance;

                    if (scaleFactor < 1f && scaleFactor > 0f)
                    {
                        if (!DOTween.IsTweening(fillBar.rectTransform))
                            fillBar.rectTransform.DOScaleX(scaleFactor, 0.025f);
                    }
                    else if (scaleFactor <= 0f)
                    {
                        if (!DOTween.IsTweening(fillBar.rectTransform))
                            fillBar.rectTransform.DOScaleX(0f, 0.025f);
                    }
                    else
                    {
                        if (!DOTween.IsTweening(fillBar.rectTransform))
                            fillBar.rectTransform.DOScaleX(1f, 0.01f);
                    }
                    break;
            }
        }
        else
        {
            if (GameManager.INSTANCE.levelManager.currentEnvironment.GetComponent<LevelProps>().GetFinishLevel())
            {
                Init(GameManager.INSTANCE.levelManager.currentEnvironment.GetComponent<LevelProps>().GetFinishLevel());
            }
        }
    }

    public void Init(Transform targetRef)
    {
        currentLevelText.text = "" + (GameManager.INSTANCE.levelManager.currentLevel + 1);
        targetLevelText.text = "" + (GameManager.INSTANCE.levelManager.currentLevel + 2);
        target = targetRef;
        fillBar.rectTransform.DOKill(false);
        fillBar.rectTransform.DOScaleX(0f, 0.025f);
        fullDistance = Vector3.Distance(player.transform.position, target.transform.position);
    }
}