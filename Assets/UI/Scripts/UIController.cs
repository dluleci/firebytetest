﻿
using UnityEngine;

public class UIController : MonoBehaviour
{
    public GameObject progress;
    public GameObject slideTutorial;
    public GameObject restart;

    [HideInInspector] public bool isStateInit = false;

    void Start()
    {
        PerformLoadState();
    }

    void Update()
    {
        if (!isStateInit)
        {
            isStateInit = true;
            switch (GameManager.INSTANCE.gameState)
            {
                case GameState.MENU:
                    PerformMenuState();
                    break;
                case GameState.PLAY:
                    PerformPlayState();
                    break;
                case GameState.WIN:
                case GameState.FAIL:
                    PerformRestart();
                    break;
            }
        }
    }

    private void PerformLoadState()
    {
        progress.SetActive(false);
        slideTutorial.SetActive(false);
        restart.SetActive(false);
    }

    private void PerformMenuState()
    {
        progress.SetActive(false);
        slideTutorial.SetActive(true);
        restart.SetActive(false);
    }

    private void PerformPlayState()
    {
        progress.SetActive(true);
        slideTutorial.SetActive(false);
    }

    private void PerformRestart()
    {
        progress.SetActive(false);
        restart.SetActive(true);
    }
}