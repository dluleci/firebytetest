
using UnityEngine;

public class EnemyAnimator : MonoBehaviour
{
    public Animator animator;

    public void Win()
    {
        animator.SetBool("isWin", true);
    }

    public void Die()
    {
        animator.SetBool("isDied", true);
    }

    public void Reset()
    {
        animator.SetBool("isWin", false);
        animator.SetBool("isDied", false);
    }
}
