
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Transform skinMesh; //needs to be refactored for next stages of game - for inventory system. This is just a temporary solution.
    public Material redMat, grayMat;

    GameManager gm;
    public float speed;
    private Transform target;
    public float smoothMovementTime;
    Rigidbody rb;
    Vector3 movement;

    bool isClampedMovementHorizontal;
    bool isAssignedAnim;

    bool isAlive = true;

   //public float turnSmoothTime;
   //float turnSmoothVelocity;

    public void OnStart()
    {
        if (!GetComponent<CapsuleCollider>().enabled)
            GetComponent<CapsuleCollider>().enabled = true;
        transform.rotation = Quaternion.identity;
        isAlive = true;
        skinMesh.GetComponent<SkinnedMeshRenderer>().material = redMat;
        rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        gm = GameManager.INSTANCE;
        target = gm.player; 
        isClampedMovementHorizontal = false;
        isAssignedAnim = false;
        GetComponent<EnemyAnimator>().Reset();
    }

    void Update()
    {
        if (gm.gameState == GameState.MENU)
        {
            if (gameObject.activeSelf)
            {
                gameObject.SetActive(false);
                transform.rotation = Quaternion.identity;
                transform.position = Vector3.zero;
            }
        }
        if (gm.gameState == GameState.PLAY && isAlive)
        {
            /*  Vector3 dirTemp = target.position - transform.position;

              Vector3 targetPosition = target.position;
            //  if (isClampedMovementHorizontal)
            //      targetPosition = new Vector3(transform.position.x, target.position.y, target.position.z);

              //transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);*/

            Vector3 direction = target.position - transform.position;


            //float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            //float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            //transform.rotation = Quaternion.Euler(0f, angle, 0f);

            direction.Normalize();
            movement = direction;
          //  MoveCharacter(movement);
        }
        if (gm.gameState == GameState.FAIL)
        {
            if (!isAssignedAnim)
            {
                isAssignedAnim = true;
                GetComponent<EnemyAnimator>().Win();
            }
        }
        
        if (GameManager.INSTANCE.gameState == GameState.FAIL || GameManager.INSTANCE.gameState == GameState.WIN)
        {
            if (rb.velocity != Vector3.zero && isAlive)
                rb.velocity = Vector3.zero;
        }
    }

    private void FixedUpdate()
    {
        if (gm.gameState == GameState.PLAY && isAlive)
        {
            MoveCharacter(movement);
        }
    }

    void MoveCharacter(Vector3 direction)
    {
        // rb.MovePosition(transform.position + (direction * speed * Time.deltaTime));
        rb.velocity = (direction * speed * 10f);
    }

    private void OnCollisionEnter(Collision collision)
    {
       /* if (collision.transform.tag.Equals(Tags.ENEMY))
        {
            isClampedMovementHorizontal = true;
        }*/
        if (collision.transform.tag.Equals(Tags.PLAYER))
        {
            target.GetComponent<PlayerController>().Die();
        }
        if (collision.transform.tag.Equals(Tags.KILLZONE))
        {
            DisableObject();
        }
        if (collision.transform.tag.Equals(Tags.OBSTACLE))
        {
            Destroy(collision.gameObject);
            Die();
            ObjectPooler.INSTANCE.SpawnFromPool("CubeBlow", transform.position, Quaternion.identity);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
       /* if (collision.transform.tag.Equals(Tags.ENEMY))
        {
            isClampedMovementHorizontal = false;
        }*/
    }

    public void Die()
    {
        GameManager.INSTANCE.player.GetComponent<PlayerController>().killCount++;
        GetComponent<CapsuleCollider>().enabled = false;
        isAlive = false;
        rb.velocity = Vector3.zero;
        rb.AddForce(Vector3.forward * 250f);
        skinMesh.GetComponent<SkinnedMeshRenderer>().material = grayMat;
        GetComponent<EnemyAnimator>().Die();
        Invoke("DisableObject", 2f);
    }

    private void DisableObject()
    {
        gameObject.SetActive(false);
        transform.rotation = Quaternion.identity;
        transform.position = Vector3.zero;
    }
}
