
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    public float offsetZ;
    public int enemyCount;
    private int spawnedEnemy;
    public float spawnTimeframe;
    bool isSpawned = false;
    float spawnedTime;

    float backupSpawnTimeframe;

    void Start()
    {
        backupSpawnTimeframe = spawnTimeframe;
        spawnedEnemy = enemyCount;
    }

    void Update()
    {
        Vector3 playerPos = GameManager.INSTANCE.player.transform.position;
        transform.position = new Vector3(transform.position.x, transform.position.y, playerPos.z - offsetZ);

        if (GameManager.INSTANCE.gameState == GameState.PLAY)
        {
            if(!isSpawned)
                GenerateEnemy();
            else
            {
                if (Time.time >= spawnTimeframe + spawnedTime)
                {
                    isSpawned = false;
                }
            }
        }
    }

    private void GenerateEnemy()
    {
        spawnTimeframe = Random.Range(backupSpawnTimeframe - 0.05f, backupSpawnTimeframe + 0.05f); //Adding Randomization for spawntimeframe
        spawnTimeframe = Mathf.Clamp(spawnTimeframe, 0f, 100f);
        isSpawned = true;
        spawnedTime = Time.time;
        int randomCountForLine = Random.Range(1, 4);
        float xPos = Random.Range(-1f, 1f);
        int multiplier = 1;
        for (int i = 0; i < randomCountForLine; i++)
        {
            if (spawnedEnemy > 0)
            {
                Vector3 spawnPos = new Vector3(xPos, transform.position.y, transform.position.z);
                ObjectPooler.INSTANCE.SpawnFromPool("Enemy", spawnPos, Quaternion.identity);
                xPos = multiplier * 0.35f;
                multiplier *= -1;
            }
            else
            {
                break;
            }
            spawnedEnemy--;
        }
    }
}
