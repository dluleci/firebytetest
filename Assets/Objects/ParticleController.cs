
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    public float defaultDestroyTime;

    public void OnStart()
    {
        Invoke("DisableObject", defaultDestroyTime);
    }

    private void DisableObject()
    {
        gameObject.SetActive(false);
        transform.rotation = Quaternion.identity;
        transform.position = Vector3.zero;
    }
}