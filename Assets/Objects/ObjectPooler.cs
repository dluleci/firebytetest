
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler INSTANCE;
    void Awake()
    {
        if (INSTANCE != null && INSTANCE != this)
        {
            Destroy(this.gameObject);
            return;
        }
        INSTANCE = this;
        DontDestroyOnLoad(this.gameObject);
    }

    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }
    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;

    void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }
            poolDictionary.Add(pool.tag, objectPool);
        }
        GameManager.INSTANCE.isAssetsLoaded = true;
    }

    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(tag))
            return null;

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;
        objectToSpawn.SetActive(true);

        if (objectToSpawn.GetComponent<Bullet>() != null)
            objectToSpawn.GetComponent<Bullet>().OnStart();

        if (objectToSpawn.GetComponent<ParticleController>() != null)
            objectToSpawn.GetComponent<ParticleController>().OnStart();

        if (objectToSpawn.GetComponent<EnemyController>() != null)
            objectToSpawn.GetComponent<EnemyController>().OnStart();

        poolDictionary[tag].Enqueue(objectToSpawn);

        return objectToSpawn;
    }
}